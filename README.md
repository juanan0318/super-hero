## Proyecto Super Héroes

#BBDD en memoria H2

 - URL: http://localhost:8080/h2
 - JDBC URL: jdbc:h2:mem:testdb
 - User: user
 - Password: user
 
#Documentación con OPEN API 3 - Swagger

 - URL: http://localhost:8080/swagger-ui/index.html#/
 - Acceso a documentación: En el buscardor de Swagger /v3/api-docs
 
#Script BBDD

 - Hay un archivo "data.sql" que insertará unos datos iniciales de prueba en la BBDD.
 
#Repositorio

 - URL de acceso: https://gitlab.com/juanan0318/super-hero.git