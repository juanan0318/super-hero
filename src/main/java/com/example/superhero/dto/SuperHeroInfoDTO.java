package com.example.superhero.dto;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
public class SuperHeroInfoDTO {

	private SuperHeroDTO superHero;
	
	private List<SuperPowerDTO> superPowers;
}
