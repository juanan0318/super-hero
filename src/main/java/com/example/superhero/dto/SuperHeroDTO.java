package com.example.superhero.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Builder
@Getter
@Setter
@ToString
public class SuperHeroDTO {

	@NotNull
	@JsonIgnore
	private Long id;
	
	@NotNull
	@Size(min = 1, max = 50)
	private String heroName;
	
	@NotNull
	@Size(min = 1, max = 50)
	private String realName;
	
	@NotNull
	private LocalDate birthDate;
		
}
