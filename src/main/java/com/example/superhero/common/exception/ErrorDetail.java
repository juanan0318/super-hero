package com.example.superhero.common.exception;

import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorDetail {

	private String message;
	private String requestDescription;
	private Date actualDate;
}
