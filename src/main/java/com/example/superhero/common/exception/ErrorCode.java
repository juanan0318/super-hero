package com.example.superhero.common.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ErrorCode {
	
	ERROR_0001("Error 0001", "No se ha encontrado ningún super héroe");
	
	private String code;
	private String description;
	
	public ResourceNotFoundException createException() {
		
		final String message = code.concat(": ").concat(description);
		return new ResourceNotFoundException(message);
	}
}
