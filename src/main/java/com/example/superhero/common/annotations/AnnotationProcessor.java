package com.example.superhero.common.annotations;

import java.util.Objects;

import org.apache.commons.lang3.ArrayUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class AnnotationProcessor {

	@Around("@annotation(CustomLogger)")
	public Object performanceLog(ProceedingJoinPoint joinPoint) throws Throwable {

		final MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
		final CustomLogger annotation = methodSignature.getMethod().getAnnotation(CustomLogger.class);
		final String calledMethodName = methodSignature.getName();
		
		if (annotation.showInputData()) {

			final String msg = "Inicio de la OP: {}. Datos de entrada: {}.";
			final Object arguments = ArrayUtils.isNotEmpty(joinPoint.getArgs()) ? joinPoint.getArgs() : "No hay parámetros de entrada";
			annotation.logLevel().printLog(msg, calledMethodName, arguments);
		}
		
		final Long startTime = System.currentTimeMillis();
		final Object proceed = joinPoint.proceed();
		final Long executionTime = System.currentTimeMillis() - startTime;
		
		if (annotation.showExecutionTime()) {
			final String msg = "La OP {} ha tardado {} milisegundos en ejecutarse.";
			annotation.logLevel().printLog(msg, calledMethodName, executionTime);
		}
		
		if (annotation.showOutputData()) {
			final String msg = "Fin de la OP: {}. Datos de salida: {}.";
			final Object arguments = Objects.nonNull(proceed) ? proceed : "No hay datos de salida";
			annotation.logLevel().printLog(msg, calledMethodName, arguments);
		}
		
		return proceed;
	}

}
