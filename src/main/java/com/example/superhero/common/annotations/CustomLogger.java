package com.example.superhero.common.annotations;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target({ METHOD })
public @interface CustomLogger {

	Logger logLevel() default Logger.INFO;
	
	boolean showInputData() default true;
	
	boolean showOutputData() default true;
	
	boolean showExecutionTime() default true;
}
