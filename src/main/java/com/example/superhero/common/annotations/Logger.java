package com.example.superhero.common.annotations;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public enum Logger {

	DEBUG, INFO, WARN, ERROR;
	
	public void printLog(String msg, Object... params) {

		switch (this) {
		case DEBUG:
			log.debug(msg, params);
			break;
		case INFO:
			log.info(msg, params);
			break;
		case ERROR:
			log.error(msg, params);
			break;
		case WARN:
			log.warn(msg, params);
			break;
		}
	}
}
