package com.example.superhero.common.config;

import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@Configuration
@OpenAPIDefinition(info = @Info(title = "API de Super Héroes", version = "1.0", description = "Mantenimiento de Super Héroes."))
public class OpenAPIConfig {

}
