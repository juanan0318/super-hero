package com.example.superhero.common.config;

import java.util.Arrays;

import org.springframework.boot.autoconfigure.cache.CacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("!test")
@Configuration
@EnableCaching
public class CacheConfig implements CacheManagerCustomizer<ConcurrentMapCacheManager>{
	

	@Override
	public void customize(ConcurrentMapCacheManager cacheManager) {
		
		cacheManager.setCacheNames(Arrays.asList("superHeroes"));
	}

}
