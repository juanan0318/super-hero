package com.example.superhero.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.superhero.common.exception.ResourceNotFoundException;
import com.example.superhero.dto.SuperHeroDTO;
import com.example.superhero.dto.SuperHeroInfoDTO;
import com.example.superhero.service.SuperHeroService;

@RestController
public class SuperHeroController {
	
	@Autowired
	private SuperHeroService service;
	
	@GetMapping("/superHeroes")
	public ResponseEntity<List<SuperHeroDTO>> getAllSuperHeroes() throws ResourceNotFoundException {
		
		final List<SuperHeroDTO> allSuperHeroes = service.getAllSuperHeroes();
		return ResponseEntity.ok(allSuperHeroes);
	}
	
	@GetMapping("superHeroes/{name}")
	public ResponseEntity<List<SuperHeroDTO>> getSuperHeroesByName(@PathVariable("name") String name) throws ResourceNotFoundException {
		
		final List<SuperHeroDTO> superHeroes = service.getSuperHeroesByName(name);
		return ResponseEntity.ok(superHeroes);
	}
	
	@GetMapping("superHero/{id}")
	public ResponseEntity<SuperHeroInfoDTO> findSuperHero(@PathVariable("id") Long id) throws ResourceNotFoundException {
		
		final SuperHeroInfoDTO superHero = service.findSuperHeroInfoById(id);
		return ResponseEntity.ok(superHero);
	}
	
	@PutMapping("superHero/{id}")
	public ResponseEntity<SuperHeroDTO> updateSuperHero(@PathVariable("id") Long id, @Valid @RequestBody SuperHeroDTO superHeroDto) throws ResourceNotFoundException {
		
		final SuperHeroDTO superHero = service.updateSuperHeroById(id, superHeroDto);
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(superHero);
	}
	
	@PostMapping("superHero")
	public ResponseEntity<SuperHeroInfoDTO> addSuperHero(@Valid @RequestBody SuperHeroInfoDTO superHeroInfoDto) throws ResourceNotFoundException {
		
		final SuperHeroInfoDTO superHeroInfo = service.addSuperHero(superHeroInfoDto);
		return ResponseEntity.status(HttpStatus.CREATED).body(superHeroInfo);
	}
	
	@DeleteMapping("superHero/{id}")
	public ResponseEntity<Void> deleteSuperHero(@PathVariable("id") Long id) throws ResourceNotFoundException {
		
		this.service.deleteSuperHeroById(id);
		return ResponseEntity.status(HttpStatus.ACCEPTED).build();
	}
	
}
