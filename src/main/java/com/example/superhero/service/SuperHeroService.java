package com.example.superhero.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.example.superhero.common.annotations.CustomLogger;
import com.example.superhero.common.exception.ErrorCode;
import com.example.superhero.common.exception.ResourceNotFoundException;
import com.example.superhero.dto.SuperHeroDTO;
import com.example.superhero.dto.SuperHeroInfoDTO;
import com.example.superhero.model.SuperHero;
import com.example.superhero.model.SuperPower;
import com.example.superhero.repository.SuperHeroRepository;

@Service
public class SuperHeroService {

	@Autowired
	private SuperHeroMapper mapper;
	
	@Autowired
	private SuperHeroRepository superHeroRepository;
	
	@CustomLogger
	@Cacheable("superHeroes")
	public List<SuperHeroDTO> getAllSuperHeroes() throws ResourceNotFoundException {
		
		final List<SuperHero> superHeroList = superHeroRepository.findAll();
				
		if (CollectionUtils.isEmpty(superHeroList)) {
			throw ErrorCode.ERROR_0001.createException();
		}
						
		return mapper.toSuperHeroList(superHeroList);
	}

	@CustomLogger
	@Cacheable("superHeroes")
	public List<SuperHeroDTO> getSuperHeroesByName(String name) throws ResourceNotFoundException {
		
		final List<SuperHero> superHeroList = superHeroRepository.findByHeroNameContainingIgnoreCase(name);
				
		if (CollectionUtils.isEmpty(superHeroList)) {
			throw ErrorCode.ERROR_0001.createException();
		}
		
		return mapper.toSuperHeroList(superHeroList);
	}

	@CustomLogger
	@Cacheable("superHeroes")
	public SuperHeroInfoDTO findSuperHeroInfoById(Long id) throws ResourceNotFoundException {
	
		final SuperHero superHero = superHeroRepository.findById(id).orElseThrow(ErrorCode.ERROR_0001::createException);
		return mapper.toSuperHeroInfo(superHero);
	}

	@CustomLogger
	@CacheEvict(value = "superHeroes", allEntries = true)
	public SuperHeroDTO updateSuperHeroById(Long id, SuperHeroDTO superHeroDto) throws ResourceNotFoundException {
		
		final SuperHero superHero = superHeroRepository.findById(id).orElseThrow(ErrorCode.ERROR_0001::createException);		
		superHero.setHeroName(superHeroDto.getHeroName());
		superHero.setRealName(superHeroDto.getRealName());
		superHero.setBirthDate(mapper.toTimestamp(superHeroDto.getBirthDate()));
		
		final SuperHero superHeroUpdated = superHeroRepository.saveAndFlush(superHero);
		return mapper.toSuperHero(superHeroUpdated);
	}
	
	@CustomLogger
	@CacheEvict(value = "superHeroes", allEntries = true)
	public SuperHeroInfoDTO addSuperHero(SuperHeroInfoDTO superHeroInfoDto) throws ResourceNotFoundException {
		
		final SuperHeroDTO superHeroDto = superHeroInfoDto.getSuperHero();
		
		SuperHero superHero = new SuperHero();		
		superHero.setHeroName(superHeroDto.getHeroName());
		superHero.setRealName(superHeroDto.getRealName());
		superHero.setBirthDate(mapper.toTimestamp(superHeroDto.getBirthDate()));
		
		final List<SuperPower> superPowers = superHeroInfoDto.getSuperPowers()
				.stream()
				.map(mapper::toSuperPower)
				.collect(Collectors.toList());
		
		superHero.setSuperPowers(superPowers);
		
		final SuperHero newSuperHero = superHeroRepository.saveAndFlush(superHero);
		return mapper.toSuperHeroInfo(newSuperHero);
	}

	@CustomLogger
	@CacheEvict(value = "superHeroes", allEntries = true)
	public void deleteSuperHeroById(Long id) throws ResourceNotFoundException {
		
		final SuperHero superHero = superHeroRepository.findById(id).orElseThrow(ErrorCode.ERROR_0001::createException);
		this.superHeroRepository.delete(superHero);
	}
		
}
