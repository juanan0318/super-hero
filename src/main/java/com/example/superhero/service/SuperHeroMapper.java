package com.example.superhero.service;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import com.example.superhero.dto.SuperHeroDTO;
import com.example.superhero.dto.SuperHeroInfoDTO;
import com.example.superhero.dto.SuperPowerDTO;
import com.example.superhero.model.SuperHero;
import com.example.superhero.model.SuperPower;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface SuperHeroMapper {

	List<SuperHeroDTO> toSuperHeroList(List<SuperHero> superHeroList);
		
	SuperHeroDTO toSuperHero(SuperHero superHero);
	
	default LocalDate toLocalDate(Timestamp timestamp) {
		
		return timestamp.toLocalDateTime().toLocalDate();
	}
	
	default Timestamp toTimestamp(LocalDate localDate) {
		
		final LocalDateTime localDateTime = localDate.atStartOfDay(ZoneId.systemDefault()).toLocalDateTime();
		return Timestamp.valueOf(localDateTime);
	}
	
	@Mappings({
		@Mapping(target = "superPowers", source = "superHero.superPowers"),
		@Mapping(target = "superHero", source = "superHero")
	})
	SuperHeroInfoDTO toSuperHeroInfo(SuperHero superHero);
	
	SuperPower toSuperPower(SuperPowerDTO superPowerDto);
}
