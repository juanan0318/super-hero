package com.example.superhero.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.example.superhero.model.SuperHero;

public interface SuperHeroRepository extends JpaRepository<SuperHero, Long> {
	
	List<SuperHero> findByHeroNameContainingIgnoreCase(@Param("name") String name);

}
