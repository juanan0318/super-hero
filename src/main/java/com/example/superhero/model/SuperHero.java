package com.example.superhero.model;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "SUPER_HERO")
@Getter
@Setter
public class SuperHero {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "hero_name", length = 50, nullable = false)
	private String heroName;
	
	@Column(name = "real_name", length = 50, nullable = false)
	private String realName;
	
    @Column(name = "birth_date", nullable = false)
    private Timestamp birthDate;
	
	@ManyToMany(cascade = CascadeType.PERSIST)
	@JoinTable(name = "SUPER_HERO_POWER", joinColumns = @JoinColumn(name = "super_hero_id"), inverseJoinColumns = @JoinColumn(name = "super_power_id"))
	private List<SuperPower> superPowers;
	
}
