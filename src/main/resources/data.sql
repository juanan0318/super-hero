INSERT INTO SUPER_HERO (id, hero_name, real_name, birth_date) VALUES (1, 'Spiderman', 'Peter Parker', TO_TIMESTAMP('2001-08-10','YYYY-MM-DD'));
INSERT INTO SUPER_HERO (id, hero_name, real_name, birth_date) VALUES (2, 'Antman', 'Scott Lang', TO_TIMESTAMP('1978-02-25','YYYY-MM-DD'));
INSERT INTO SUPER_HERO (id, hero_name, real_name, birth_date) VALUES (3, 'Ironman', 'Tony Stark', TO_TIMESTAMP('1969-05-09','YYYY-MM-DD'));
INSERT INTO SUPER_HERO (id, hero_name, real_name, birth_date) VALUES (4, 'Capitana Marvel', 'Carol Danvers', TO_TIMESTAMP('1980-01-05','YYYY-MM-DD'));
INSERT INTO SUPER_HERO (id, hero_name, real_name, birth_date) VALUES (5, 'Capitán América', 'Steve Rogers', TO_TIMESTAMP('1918-11-10','YYYY-MM-DD'));

INSERT INTO SUPER_POWER (id, name, description) VALUES (1, 'Fuerza', 'Fuerza fuera de lo normal');
INSERT INTO SUPER_POWER (id, name, description) VALUES (2, 'Vuelo', 'Capacidad de volar');
INSERT INTO SUPER_POWER (id, name, description) VALUES (3, 'Trepar', 'Capacidad de trepar por las paredes');
INSERT INTO SUPER_POWER (id, name, description) VALUES (4, 'Inteligencia', 'Inteligencia fuera de lo normal');
INSERT INTO SUPER_POWER (id, name, description) VALUES (5, 'Cambio de Tamaño', 'Capacidad de cambiar de tamaño objetos u otros');


INSERT INTO SUPER_HERO_POWER (super_hero_id, super_power_id) VALUES (1, 1);
INSERT INTO SUPER_HERO_POWER (super_hero_id, super_power_id) VALUES (1, 3);
INSERT INTO SUPER_HERO_POWER (super_hero_id, super_power_id) VALUES (2, 5);
INSERT INTO SUPER_HERO_POWER (super_hero_id, super_power_id) VALUES (3, 4);
INSERT INTO SUPER_HERO_POWER (super_hero_id, super_power_id) VALUES (4, 1);
INSERT INTO SUPER_HERO_POWER (super_hero_id, super_power_id) VALUES (4, 2);
INSERT INTO SUPER_HERO_POWER (super_hero_id, super_power_id) VALUES (5, 1);








