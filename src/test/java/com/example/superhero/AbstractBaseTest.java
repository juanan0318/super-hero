package com.example.superhero;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import com.example.superhero.dto.SuperHeroDTO;
import com.example.superhero.dto.SuperHeroInfoDTO;
import com.example.superhero.dto.SuperPowerDTO;
import com.example.superhero.model.SuperHero;
import com.example.superhero.model.SuperPower;

public abstract class AbstractBaseTest {

	protected static final Long LONG = Long.valueOf(1L);
	protected static final String VALUE = String.valueOf("Value");
	protected static final String MSG_KO = String.valueOf("Recurso no encontrado");
	private static final LocalDate DATE = LocalDate.of(2010, 5, 3);
	
	protected SuperHeroDTO buildSuperHeroDTO() {
		
		return SuperHeroDTO.builder().heroName(VALUE).realName(VALUE).birthDate(DATE).build();
	}
	
	protected SuperPowerDTO buildSuperPowerDTO() {
		
		return SuperPowerDTO.builder().name(VALUE).description(VALUE).build();
	}
	
	protected List<SuperHeroDTO> buildSuperHeroDTOList() {
		
		return Arrays.asList(buildSuperHeroDTO());
	}
	
	protected SuperHeroInfoDTO buildSuperHeroInfoDTO() {
		
		final SuperHeroDTO superHeroDTO = buildSuperHeroDTO();
		final SuperPowerDTO superPowerDTO = buildSuperPowerDTO();
		
		return SuperHeroInfoDTO.builder().superHero(superHeroDTO).superPowers(Arrays.asList(superPowerDTO)).build();
	}
	
	protected List<SuperHero> buildSuperHeroList() {
		
		return Arrays.asList(buildSuperHero());
	}

	protected SuperHero buildSuperHero() {
		
		SuperHero superHero = new SuperHero();
		superHero.setId(LONG);
		superHero.setHeroName(VALUE);
		superHero.setRealName(VALUE);
		superHero.setBirthDate(new Timestamp(LONG));
		superHero.setSuperPowers(Arrays.asList(buildSuperPower()));
		
		return superHero;
	}

	protected SuperPower buildSuperPower() {
		
		SuperPower superPower = new SuperPower();
		superPower.setId(LONG);
		superPower.setName(VALUE);
		superPower.setDescription(VALUE);
		
		return superPower;
	}
}
