package com.example.superhero.integration;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;

import com.example.superhero.AbstractBaseTest;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@ActiveProfiles("test")
public class SuperHeroControllerIT extends AbstractBaseTest {
	
	private static final ResultMatcher OK = MockMvcResultMatchers.status().isOk();
	private static final ResultMatcher NOT_FOUND = MockMvcResultMatchers.status().isNotFound();
	private static final ResultMatcher ACCEPTED = MockMvcResultMatchers.status().isAccepted();
	private static final ResultMatcher CREATED = MockMvcResultMatchers.status().isCreated();
	
	@Autowired
	private MockMvc mvc;
	
	@Autowired
	private ObjectMapper mapper;
	
	@Test
	public void getAllSuperHeroesCaseOK() throws Exception {
		
		this.mvc.perform(MockMvcRequestBuilders.get("/superHeroes")).andExpect(OK);
	}
	
	@Test
	public void getSuperHeroesByNameCaseOK() throws Exception {
		
		this.mvc.perform(MockMvcRequestBuilders.get("/superHeroes/{name}", "Man")).andExpect(OK);
	}

	@Test
	public void getSuperHeroesByNameCaseNotFound() throws Exception {
		
		this.mvc.perform(MockMvcRequestBuilders.get("/superHeroes/{name}", "Other")).andExpect(NOT_FOUND);
	}
	
	@Test
	public void findSuperHeroCaseOK() throws Exception {
		
		this.mvc.perform(MockMvcRequestBuilders.get("/superHero/{id}", Long.valueOf(1L))).andExpect(OK);
	}

	@Test
	public void findSuperHeroCaseNotFound() throws Exception {
		
		this.mvc.perform(MockMvcRequestBuilders.get("/superHero/{id}", Long.valueOf(-1L))).andExpect(NOT_FOUND);
	}
	
	@Test
	public void updateSuperHeroCaseOK() throws Exception {
		
		this.mvc.perform(MockMvcRequestBuilders.put("/superHero/{id}", 1L)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(super.buildSuperHeroDTO())))
			.andExpect(ACCEPTED);
	}
	
	@Test
	public void updateSuperHeroCaseNotFound() throws Exception {
		
		this.mvc.perform(MockMvcRequestBuilders.put("/superHero/{id}", -1L)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(super.buildSuperHeroDTO())))
			.andExpect(NOT_FOUND);
	}
	
	@Test
	public void addSuperHeroCaseOK() throws Exception {
		
		this.mvc.perform(MockMvcRequestBuilders.post("/superHero")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(super.buildSuperHeroInfoDTO())))
			.andExpect(CREATED);
	}
	
	@Test
	public void deleteSuperHeroCaseOK() throws Exception {
		
		this.mvc.perform(MockMvcRequestBuilders.delete("/superHero/{id}", 1L)).andExpect(ACCEPTED);
	}
	
	@Test
	public void deleteSuperHeroCaseNotFound() throws Exception {
		
		this.mvc.perform(MockMvcRequestBuilders.delete("/superHero/{id}", -1L)).andExpect(NOT_FOUND);
	}
}
