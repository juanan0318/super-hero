package com.example.superhero.unit;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import com.example.superhero.AbstractBaseTest;
import com.example.superhero.common.exception.ResourceNotFoundException;
import com.example.superhero.controller.SuperHeroController;
import com.example.superhero.dto.SuperHeroDTO;
import com.example.superhero.dto.SuperHeroInfoDTO;
import com.example.superhero.service.SuperHeroService;

@SpringBootTest
@ActiveProfiles("test")
public class SuperHeroControllerTest extends AbstractBaseTest {

	@Autowired
	private SuperHeroController superHeroController;
	
	@MockBean
	private SuperHeroService superHeroService;

	@Test
	public void getAllSuperHeroes() throws ResourceNotFoundException {
		
		ResponseEntity<List<SuperHeroDTO>> superHeroes = superHeroController.getAllSuperHeroes();
		assertThat(superHeroes.getStatusCode()).isEqualTo(HttpStatus.OK);
	}
	
	@Test
	public void getSuperHeroesByName() throws ResourceNotFoundException {
		
		ResponseEntity<List<SuperHeroDTO>> superHeroes = superHeroController.getSuperHeroesByName(VALUE);
		assertThat(superHeroes.getStatusCode()).isEqualTo(HttpStatus.OK);
	}
	
	@Test
	public void findSuperHero() throws ResourceNotFoundException {
		
		ResponseEntity<SuperHeroInfoDTO> superHeroes = superHeroController.findSuperHero(Long.valueOf(1L));
		assertThat(superHeroes.getStatusCode()).isEqualTo(HttpStatus.OK);
	}
	
	@Test
	public void updateSuperHero() throws ResourceNotFoundException {
		
		ResponseEntity<SuperHeroDTO> superHeroes = superHeroController.updateSuperHero(LONG, buildSuperHeroDTO());
		assertThat(superHeroes.getStatusCode()).isEqualTo(HttpStatus.ACCEPTED);
	}
	
	@Test
	public void addSuperHero() throws ResourceNotFoundException {
		
		ResponseEntity<SuperHeroInfoDTO> superHeroInfo = superHeroController.addSuperHero(super.buildSuperHeroInfoDTO());
		assertThat(superHeroInfo.getStatusCode()).isEqualTo(HttpStatus.CREATED);
	}
	
	@Test
	public void deleteSuperHero() throws ResourceNotFoundException {
		
		ResponseEntity<Void> superHeroes = superHeroController.deleteSuperHero(LONG);
		assertThat(superHeroes.getStatusCode()).isEqualTo(HttpStatus.ACCEPTED);
	}
	
}
