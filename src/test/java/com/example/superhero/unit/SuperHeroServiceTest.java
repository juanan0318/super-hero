package com.example.superhero.unit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import com.example.superhero.AbstractBaseTest;
import com.example.superhero.common.exception.ResourceNotFoundException;
import com.example.superhero.dto.SuperHeroDTO;
import com.example.superhero.dto.SuperHeroInfoDTO;
import com.example.superhero.repository.SuperHeroRepository;
import com.example.superhero.service.SuperHeroMapper;
import com.example.superhero.service.SuperHeroService;

@SpringBootTest
@ActiveProfiles("test")
public class SuperHeroServiceTest extends AbstractBaseTest {
	
	@Autowired
	private SuperHeroService superHeroService;
	
	@Spy
	private SuperHeroMapper superHeroMapper = Mappers.getMapper(SuperHeroMapper.class);
	
	@MockBean
	private SuperHeroRepository superHeroRepository;
	
	@BeforeEach
	public void init() {
		
		when(superHeroRepository.findAll()).thenReturn(super.buildSuperHeroList());
		when(superHeroRepository.findByHeroNameContainingIgnoreCase(Mockito.anyString())).thenReturn(super.buildSuperHeroList());
		when(superHeroRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(super.buildSuperHero()));
		when(superHeroRepository.saveAndFlush(Mockito.any())).thenReturn(super.buildSuperHero());
	}
	
	@Test
	public void getAllSuperHeroesCaseOK() throws ResourceNotFoundException {
		
		List<SuperHeroDTO> superHeroes = superHeroService.getAllSuperHeroes();
		assertThat(superHeroes).isNotEmpty();
	}
	
	@Test
	public void getAllSuperHeroesCaseNotFound() throws ResourceNotFoundException {
		
		when(superHeroRepository.findAll()).thenReturn(null);
		assertThrows(ResourceNotFoundException.class, superHeroService::getAllSuperHeroes, MSG_KO);
	}
	
	@Test
	public void getSuperHeroesByNameCaseOK() throws ResourceNotFoundException {
		
		List<SuperHeroDTO> superHeroes = superHeroService.getSuperHeroesByName(VALUE);
		assertThat(superHeroes).isNotEmpty();
	}
	
	@Test
	public void getSuperHeroesByNameCaseNotFound() throws ResourceNotFoundException {
		
		when(superHeroRepository.findByHeroNameContainingIgnoreCase(Mockito.anyString())).thenReturn(null);
		assertThrows(ResourceNotFoundException.class, () -> superHeroService.getSuperHeroesByName(VALUE), MSG_KO);
	}
	
	@Test
	public void findSuperHeroInfoByIdCaseOK() throws ResourceNotFoundException {
		
		SuperHeroInfoDTO superHeroInfoDTO = superHeroService.findSuperHeroInfoById(LONG);
		assertThat(superHeroInfoDTO).isNotNull();
	}
	
	@Test
	public void findSuperHeroInfoByIdCaseNotFound() throws ResourceNotFoundException {
		
		when(superHeroRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
		assertThrows(ResourceNotFoundException.class, () -> superHeroService.findSuperHeroInfoById(LONG), MSG_KO);
	}
	
	@Test
	public void updateSuperHeroByIdCaseOK() throws ResourceNotFoundException {
		
		SuperHeroDTO superHeroDTO = superHeroService.updateSuperHeroById(LONG, super.buildSuperHeroDTO());
		assertThat(superHeroDTO).isNotNull();
	}
	
	@Test
	public void updateSuperHeroByIdCaseNotFound() throws ResourceNotFoundException {
		
		when(superHeroRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
		assertThrows(ResourceNotFoundException.class, () -> superHeroService.updateSuperHeroById(LONG, super.buildSuperHeroDTO()), MSG_KO);
	}
	
	@Test
	public void addSuperHeroCaseOK() throws ResourceNotFoundException {
		
		SuperHeroInfoDTO superHeroInfoDto = superHeroService.addSuperHero(super.buildSuperHeroInfoDTO());
		assertThat(superHeroInfoDto).isNotNull();
	}
	
	@Test
	public void deleteSuperHeroByIdCaseOK() throws ResourceNotFoundException {
		
		this.superHeroService.deleteSuperHeroById(LONG);
	}
	
	@Test
	public void deleteSuperHeroByIdNotFound() throws ResourceNotFoundException {
		
		when(superHeroRepository.findById(Mockito.any())).thenReturn(Optional.empty());
		assertThrows(ResourceNotFoundException.class, () -> superHeroService.deleteSuperHeroById(LONG), MSG_KO);
	}

}
